from SniffLib.Driver.mesh import Mesh, mesh_package
from SniffLib.Driver.commands import Commands
from SniffLib.Util.statusprint import *

import time
from sys import stdout
from distutils import util

class set_output:
    def __init__(self, ctr : Mesh):
        self.ctr = ctr

    def run(self, argv : list):
        if(len(argv) == 2):
            index = int(argv[0])
            state = util.strtobool(argv[1])
            print("Setting index: " + str(index) + " state: " + str(state))
            self.ctr.mesh_set(index, Commands.group_output_state, [state])
        elif(len(argv) == 3):
            index = int(argv[0])
            state = util.strtobool(argv[1])
            level = float(argv[2])
            if level > 100 or level < 0:
                raise Exception("Level must be between 0-100")
            print("Setting index: " + str(index) + " state: " + str(state) + " level: " + str(level) +"%")

            level_true = int(0xffff*level/100)
            self.ctr.mesh_set(index, Commands.group_output_state_and_level, [state, level_true&0xff, (level_true>>8)&0xff])
        else:
            raise Exception("Usage: set_output INDEX STATE [LEVEL]")
