from SniffLib.Driver.mesh import Mesh, mesh_package
from SniffLib.Driver.commands import Commands
from SniffLib.Util.statusprint import *

import time
from sys import stdout

class uptime:
    def __init__(self, ctr : Mesh):
        self.ctr = ctr

    def run(self, argv : list):
        if(len(argv) == 1):
            index = int(argv[0])
        else:
            index = 17

        stdout.write("Send RTR for uptime")
        stdout.flush()
        response1 = self.ctr.mesh_rtr(index, Commands.get_cycles_since_boot, [])
        
        if response1 == None:
            print(CLR_RED + " No response" + CLR_OFF)
            return False
        else:
            print(CLR_GREEN + " Got response" + CLR_OFF)
        
        uptime = response1.payload[3] + (response1.payload[2] << 8) + (response1.payload[1] << 16) + (response1.payload[0] << 24)

        uptime_1 = uptime / 50
        print("Got uptime: " + str(uptime_1))
        
        print("Wait 5s")
        time.sleep(5)

        stdout.write("Send RTR for uptime")
        stdout.flush()
        response2 = self.ctr.mesh_rtr(index, Commands.get_cycles_since_boot, [])
        
        if response2 == None:
            print(CLR_RED + " No response" + CLR_OFF)
            return False
        else:
            print(CLR_GREEN + " Got response" + CLR_OFF)
        
        uptime = response2.payload[3] + (response2.payload[2] << 8) + (response2.payload[1] << 16) + (response2.payload[0] << 24)
        
        uptime_2 = uptime / 50
        print("Got uptime: " + str(uptime_2))

        diff = uptime_2 - uptime_1
        if (diff > 10) or (diff < 2):
            print("Invalid diff: " + str(diff))
            return False 

        print("Test passed with diff: " + str(diff))
        return True

