from SniffLib.Driver.mesh import Mesh, mesh_package
from SniffLib.Driver.commands import Commands
from SniffLib.Util.statusprint import *

import time
from sys import stdout
from distutils import util
from si_prefix import si_format

class cpu_load:
    def __init__(self, ctr : Mesh):
        self.ctr = ctr

    def run(self, argv : list):
        if(len(argv) == 1):
            index = int(argv[0])
            print("Getting CPU load for: " + str(index))
            response = self.ctr.mesh_rtr(index, Commands.get_cpu_load, [])

            max_cycles_1s = (response.payload[0] << 8) + response.payload[1]
            min_cycles_1s = (response.payload[2] << 8) + response.payload[3]
            avg_cycles_1s = (response.payload[4] << 8) + response.payload[5]

            max_cycles_20ms = response.payload[6]
            min_cycles_20ms = response.payload[7]
            avg_cycles_20ms = response.payload[8]

            print("Cycles per 1s:")
            print("   Max:" + str(max_cycles_1s))
            print("   Min:" + str(min_cycles_1s))
            print("   Avg:" + str(avg_cycles_1s))

            print("Cycles per 20ms:")
            print("   Max:" + str(max_cycles_20ms))
            print("   Min:" + str(min_cycles_20ms))
            print("   Avg:" + str(avg_cycles_20ms))

            print("Frequency:  ")
            #mean = (avg_cycles_1s + (avg_cycles_20ms*50))/2
            mean = avg_cycles_20ms*50
            print("   " + si_format(mean, precision=2) + "Hz")

            #print("Resetting CPU load for: " + str(index))
            #self.ctr.mesh_set(index, Commands.get_cpu_load, [])
        else:
            raise Exception("Usage: cpu_load INDEX")
