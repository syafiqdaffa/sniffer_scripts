from SniffLib.Driver.mesh import Mesh, mesh_package
from SniffLib.Driver.MiniPackage import MiniPackage
from SniffLib.Driver.commands import Commands
from SniffLib.Util.statusprint import *

import time
from sys import stdout
from distutils import util

USAGE = "Usage: tunable_white INDEX OUTPUT [MIN_TEMP MAX_TEMP]"

class tunable_white:
    def __init__(self, ctr : Mesh):
        self.ctr = ctr

    def __fixed(self):
        index = 12
        output = 0
        min_temp = 1800
        max_temp = 5000
        slewrate = 32
        mp = MiniPackage()
        mp.type = 0x01

            
        avg_temp = int((min_temp + max_temp)/2)
        min_temp_payload = [(min_temp>>8)&0xff, min_temp&0xff]
        max_temp_payload = [(max_temp>>8)&0xff, max_temp&0xff]
        avg_temp_payload = [(avg_temp>>8)&0xff, avg_temp&0xff]
        slewrate_payload = [(slewrate>>8)&0xff, slewrate&0xff]

        # Make sure light is on
        print("Setting output to max")
        self.ctr.mesh_set(index, Commands.group_output_state_and_level, [1, 0xff, 0xff])
        input("Press Enter to continue...")


        # Configure TW to min
        print("Configuring Fixed TW: min")
        print("    Max: " + str(max_temp) + "K")
        print("    Min: " + str(min_temp) + "K")
        print("    Fix: " + str(min_temp) + "K")

        payload = [output]
        mp.data = [min_temp_payload, max_temp_payload, slewrate_payload, min_temp_payload]
        payload.extend(mp.to_payload())

        self.ctr.mesh_set(index, 0x100, payload)
        input("Press Enter to continue...")

        # Configure TW to middle
        print("Configuring Fixed TW: middle")
        print("    Max: " + str(max_temp) + "K")
        print("    Min: " + str(min_temp) + "K")
        print("    Fix: " + str(avg_temp) + "K")

        payload = [output]
        mp.data = [min_temp_payload, max_temp_payload, slewrate_payload, avg_temp_payload]
        payload.extend(mp.to_payload())

        self.ctr.mesh_set(index, 0x100, payload)
        input("Press Enter to continue...")

        # Configure TW to max
        print("Configuring Fixed TW: max")
        print("    Max: " + str(max_temp) + "K")
        print("    Min: " + str(min_temp) + "K")
        print("    Fix: " + str(max_temp) + "K")

        payload = [output]
        mp.data = [min_temp_payload, max_temp_payload, slewrate_payload, max_temp_payload]
        payload.extend(mp.to_payload())

        self.ctr.mesh_set(index, 0x100, payload)
        input("Press Enter to continue...")


        # Testing override, empty payload
        print("Setting override to min: " + str(min_temp) + "K")
        payload = [output, 1]
        payload.extend(min_temp_payload)
        self.ctr.mesh_set(index, 0x101, payload)
        input("Press Enter to continue...")

        print("Disabling override by empty payload")
        payload = [output, 1]
        self.ctr.mesh_set(index, 0x101, payload)
        input("Press Enter to continue...")


        # Testing override, 0 payload
        print("Setting override to min: " + str(min_temp) + "K")
        payload = [output, 1]
        payload.extend(min_temp_payload)
        self.ctr.mesh_set(index, 0x101, payload)
        input("Press Enter to continue...")

        print("Disabling override by 0 payload")
        payload = [output, 0, 0]
        self.ctr.mesh_set(index, 0x101, payload)
        input("Press Enter to continue...")

        # Testing override, changing light level
        print("Setting override to min: " + str(min_temp) + "K")
        payload = [output, 1]
        payload.extend(min_temp_payload)
        self.ctr.mesh_set(index, 0x101, payload)
        input("Press Enter to continue...")

        print("Disabling override by 0xfffe light level")
        self.ctr.mesh_set(index, Commands.group_output_state_and_level, [1, 0xfe, 0xff])
        input("Press Enter to continue...")

        # Testing override, toggling light
        print("Setting override to min: " + str(min_temp) + "K")
        payload = [output, 1]
        payload.extend(min_temp_payload)
        self.ctr.mesh_set(index, 0x101, payload)
        input("Press Enter to continue...")

        print("Disabling override by toggling light")
        self.ctr.mesh_set(index, Commands.group_output_state, [0])
        self.ctr.mesh_set(index, Commands.group_output_state, [1])
        input("Press Enter to continue...")

    def __adjust_keep(self):
        index = 12
        output = 0
        min_temp = 1800
        max_temp = 5000
        slewrate = 32
        mp = MiniPackage()
        mp.type = 0x02

            
        avg_temp = int((min_temp + max_temp)/2)
        min_temp_payload = [(min_temp>>8)&0xff, min_temp&0xff]
        max_temp_payload = [(max_temp>>8)&0xff, max_temp&0xff]
        avg_temp_payload = [(avg_temp>>8)&0xff, avg_temp&0xff]
        slewrate_payload = [(slewrate>>8)&0xff, slewrate&0xff]

        # Make sure light is on
        print("Setting output to max")
        self.ctr.mesh_set(index, Commands.group_output_state_and_level, [1, 0xff, 0xff])
        input("Press Enter to continue...")


        # Configure TW to middle
        print("Configuring Adjust keep: middle")
        print("    Max: " + str(max_temp) + "K")
        print("    Min: " + str(min_temp) + "K")
        print("    Fix: " + str(avg_temp) + "K")

        payload = [output]
        mp.data = [min_temp_payload, max_temp_payload, slewrate_payload, avg_temp_payload]
        payload.extend(mp.to_payload())

        self.ctr.mesh_set(index, 0x100, payload)
        input("Press Enter to continue...")


        # Changing temp to min
        print("Setting temp to min: " + str(min_temp) + "K")
        payload = [output, 1]
        payload.extend(min_temp_payload)
        self.ctr.mesh_set(index, 0x101, payload)
        input("Press Enter to continue...")

        # Changing temp to max
        print("Setting temp to max: " + str(max_temp) + "K")
        payload = [output, 1]
        payload.extend(max_temp_payload)
        self.ctr.mesh_set(index, 0x101, payload)
        input("Press Enter to continue...")


    def __adjust_reset(self):
        index = 12
        output = 0
        min_temp = 1800
        max_temp = 5000
        slewrate = 32
        mp = MiniPackage()
        mp.type = 0x03

            
        avg_temp = int((min_temp + max_temp)/2)
        min_temp_payload = [(min_temp>>8)&0xff, min_temp&0xff]
        max_temp_payload = [(max_temp>>8)&0xff, max_temp&0xff]
        avg_temp_payload = [(avg_temp>>8)&0xff, avg_temp&0xff]
        slewrate_payload = [(slewrate>>8)&0xff, slewrate&0xff]

        # Make sure light is on
        print("Setting output to max")
        self.ctr.mesh_set(index, Commands.group_output_state_and_level, [1, 0xff, 0xff])
        input("Press Enter to continue...")


        # Configure TW to min
        print("Configuring Adjust keep: min")
        print("    Max: " + str(max_temp) + "K")
        print("    Min: " + str(min_temp) + "K")
        print("    Fix: " + str(min_temp) + "K")

        payload = [output]
        mp.data = [min_temp_payload, max_temp_payload, slewrate_payload, min_temp_payload]
        payload.extend(mp.to_payload())

        self.ctr.mesh_set(index, 0x100, payload)
        input("Press Enter to continue...")


        # Changing temp to max
        print("Setting temp to max: " + str(max_temp) + "K")
        payload = [output, 1]
        payload.extend(max_temp_payload)
        self.ctr.mesh_set(index, 0x101, payload)
        input("Press Enter to continue...")


    def __dim_to_warm(self):
        index = 12
        output = 0
        min_temp = 1800
        max_temp = 5000
        slewrate = 0
        dim_level_from = 70
        dim_level_to = 90
        log_factor = 127

        mp = MiniPackage()
        mp.type = 0x04

            
        #min_temp_payload = [min_temp&0xff, (min_temp>>8)&0xff]
        #max_temp_payload = [max_temp&0xff, (max_temp>>8)&0xff]
        #slewrate_payload = [slewrate&0xff, (slewrate>>8)&0xff]

        min_temp_payload = int(min_temp>>6) & 0xff
        max_temp_payload = int(max_temp>>6) & 0xff
        slewrate_payload = int(slewrate>>2) & 0xff
        dim_level_from_payload = int(dim_level_from*255/100)
        dim_level_to_payload = int(dim_level_to*255/100)
        log_factor_payload = log_factor

        # Make sure light is on
        print("Setting output to max")
        self.ctr.mesh_set(index, Commands.group_output_state_and_level, [1, 0xff, 0xff])
        input("Press Enter to continue...")


        # Configure TW to min
        print("Configuring dim-to-warm:")
        print("    Max:            " + str(max_temp) + "K")
        print("    Min:            " + str(min_temp) + "K")
        print("    Slew:           " + str(slewrate) + "K/20ms")
        print("    Dim level from: " + str(dim_level_from) + "%")
        print("    Dim level to:   " + str(dim_level_to) + "%")
        print("    Log factor:     " + str(log_factor))

        payload = [output]
        mp.data = [
                min_temp_payload, 
                max_temp_payload, 
                slewrate_payload, 
                dim_level_from_payload,
                dim_level_to_payload,
                log_factor_payload
        ]
        payload.extend(mp.to_payload())
        self.ctr.mesh_set(index, 0x100, payload)
        input("Press Enter to continue...")

        print("Switching to and from via direct access")
        mp_tmp = MiniPackage()
        payload = [output]

        mp_tmp.type = 0x23 # Dim to warm from temp
        mp_tmp.data = [((dim_level_to_payload >> 8) & 0xff), (dim_level_to_payload & 0xff)]
        payload.extend(mp_tmp.to_payload())

        mp_tmp.type = 0x24 # Dim to warm to temp
        mp_tmp.data = [((dim_level_from_payload >> 8) & 0xff), (dim_level_from_payload & 0xff)]
        payload.extend(mp_tmp.to_payload())

        del mp_tmp

        self.ctr.mesh_set(index, 0x100, payload)
        input("Press Enter to continue...")
        


    def run(self, argv : list):
        if len(argv) != 1:
            raise Exception("Must specify one of: fixed, dim_to_warm or astro")

        string = str(argv[0])

        if string == "fixed":
            self.__fixed()
        elif string == "adjust_keep":
            self.__adjust_keep()
        elif string == "adjust_reset":
            self.__adjust_reset()
        elif string == "dim_to_warm":
            self.__dim_to_warm()
        elif string == "astro":
            raise Exception("Astro not implemented")
        else:
            raise Exception("Unsupported test, must specify one of: fixed, dim_to_warm or astro")
