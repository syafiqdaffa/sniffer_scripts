from SniffLib.Driver.mesh import Mesh, mesh_package
from SniffLib.Driver.MiniPackage import MiniPackage
from SniffLib.Driver.commands import Commands
from SniffLib.Util.statusprint import *

import time
from sys import stdout
from distutils import util

USAGE = "Usage: setOutput ..."

class setOutput:
    def __init__(self, ctr : Mesh):
        self.ctr = ctr


    def __state(self, argv):
        index = 17
        output = 0
        payload = []
        source_package = MiniPackage()
        source_package.type = 0x03
        state_package = MiniPackage()
        state_package.type = 0x00

        if len(argv) == 1:
            state = util.strtobool(argv[0])
        else:
            raise Exception("Specify only [state:boolean]")

        source_package.data = [1]
        payload.extend(source_package.to_payload())

        state_package.data = [state]
        payload.extend(state_package.to_payload())

        self.ctr.mesh_set(index, Commands.output_set, payload)
        input("Press Enter to continue...")


    def __level(self, argv):
        index = 17
        output = 0
        payload = []
        source_package = MiniPackage()
        source_package.type = 0x03
        level_package = MiniPackage()
        level_package.type = 0x00

        if len(argv) == 1:
            level = int(argv[0])
            if level < 0 or level > 100:
                raise Exception("Specify only [level:0-100]")
            level = int(level * 0xffff / 100)
        else:
            raise Exception("Specify only [level:0-100]")


        source_package.data = [1]
        payload.extend(source_package.to_payload())

        level_package.data = [(level>>8)&0xff, level&0xff]
        payload.extend(level_package.to_payload())

        self.ctr.mesh_set(index, Commands.output_set, payload)
        input("Press Enter to continue...")


    def __state_n_level(self, argv):
        index = 17
        output = 0
        payload = []
        source_package = MiniPackage()
        source_package.type = 0x03
        snl_package = MiniPackage()
        snl_package.type = 0x00

        if len(argv) == 2:
            state = util.strtobool(argv[0])
            level = int(argv[1])
            if level < 0 or level > 100:
                raise Exception("Specify only [state:boolean] [level:0-100]")
            level = int(level * 0xffff / 100)
        else:
            raise Exception("Specify only [state:boolean] [level:0-100]")

        source_package.data = [1]
        payload.extend(source_package.to_payload())

        snl_package.data = [state, (level>>8)&0xff, level&0xff]
        payload.extend(snl_package.to_payload())

        self.ctr.mesh_set(index, Commands.output_set, payload)
        input("Press Enter to continue...")


    def run(self, argv : list):
        if len(argv) <= 1:
            raise Exception("Must specify test")

        string = str(argv[0])

        if string == "state":
            self.__state(argv[1:])
        elif string == "level":
            self.__level(argv[1:])
        elif string == "state_n_level":
            self.__state_n_level(argv[1:])
        else:
            raise Exception("Unsupported test")
